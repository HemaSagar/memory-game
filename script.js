const gameContainer = document.getElementById("game");
const home = document.getElementById("home");
const gameStarted = document.getElementById("game-started");
const startGame = document.getElementById("start-game");
const winScreen = document.getElementById("win");
const playAgain = document.getElementById("play-again");
const restart = document.getElementById("restart");
const restartWarning = document.getElementById("restart-warning");
const restartYes = document.getElementById("restart-yes");
const restartNo = document.getElementById("restart-no");
const bestScore = document.getElementById("best-score");

const gifs = new Array(12).fill(0).map((value, index) => {
  return `${index + 1}.gif`;
})

gifs.push(...gifs);

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledGifs = shuffle(gifs);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForGifs(gifs) {
  for (let gif of gifs) {
    // for(i=0;i<1;i++){
    // create a new div
    const newDiv = document.createElement("div");
    const frontFace = document.createElement("div");
    const backFace = document.createElement("div");
    const backImg = document.createElement("img");
    const frontImg = document.createElement("img");
    // give it a class attribute for the value we are looping over
    newDiv.classList.add("card", gif);
    frontFace.classList.add("face", "front-face");
    backFace.classList.add("face", "back-face");

    frontImg.setAttribute("src", `./images/guess.jpg`);
    backImg.setAttribute("src", `./gifs/${gif}`);
    frontFace.append(frontImg);
    backFace.append(backImg);
    newDiv.append(frontFace, backFace);
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick, false);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  let currElement = null;
  if (event.target.tagName === "IMG") {
    currElement = event.target.parentElement.parentElement;
  }
  else {
    currElement = event.target.parentElement;
  }
  if (lockFlipping || currElement.classList.contains('flipped')) {
    return;
  }
  updateScore();
  flipCards(currElement);
  if (flippedNumber === 2) {
    matchwithPrevious(firstCard, secondCard);
  }
}

function flipCards(currElement) {
  flippedNumber += 1;
  currElement.classList.toggle('flipped');

  if (!flipped) {
    firstCard = currElement;
    flipped = true;
  }
  else {
    secondCard = currElement;
    flipped = false;

  }
}

function matchwithPrevious(firstCard, secondCard) {
  if (firstCard.classList[1] === secondCard.classList[1]) {
    removeEventListeners(firstCard, secondCard);
    cardsMatched += 1;
    storeBestScore(score, cardsMatched);
  }
  else {
    closeOpenedCards(firstCard, secondCard);
  }
  if (cardsMatched === 12) {
    displayHome();
  }
  flippedNumber = 0;
}

function removeEventListeners(firstElement, secondElement) {
  firstElement.removeEventListener('click', handleCardClick);
  secondElement.removeEventListener('click', handleCardClick);
}

function closeOpenedCards(firstCard, secondCard) {
  lockFlipping = true;
  setTimeout(() => {
    firstCard.classList.toggle('flipped');
    secondCard.classList.toggle('flipped');
    lockFlipping = false;
  }, 1 * 1000)
}


function updateScore(restart) {
  score += 1;
  if (restart) {
    score = 0;
  }
  document.getElementById('live-score').innerHTML = `Moves :${score}`;

}

function storeBestScore(score, cardsMatched) {
  if (localStorage.getItem('bestScore')) {
    let best = localStorage.getItem('bestScore');
    if(best !=="undefined"){
      setBestScore(best);
      bestScore.style.visibility="visible";
    }
    if ((Number(best) > score || best ==="undefined") && cardsMatched === 12) {
      localStorage.setItem('bestScore', score);
      setBestScore(score);
    }
    
  }
  else {
    localStorage.setItem('bestScore', undefined);
  }
}

function setBestScore(best) {

  document.getElementById('best-score').innerText = `Best: ${best}`;
}

function displayHome() {
  document.getElementById("winScore").innerText = `${score}`;
  document.getElementById("winBestScore").innerText  = `${localStorage.getItem('bestScore')}`;
  gameStarted.style.display = "none";
  winScreen.style.display = "flex";
}

function restartGame() {
  gameStarted.style.display="none";
  restartWarning.style.display="flex";
  
}
function reset(){
  score = 0;
  cardsMatched = 0;
  updateScore(true);
  gameContainer.innerHTML = "";
  createDivsForGifs(shuffle(gifs));
}
function confirmRestart(event) {
  if(event.target.id==="restart-yes"){
    reset();
  }
  gameStarted.style.display="block";
  restartWarning.style.display = "none";
}
restartYes.addEventListener('click',confirmRestart);
restartNo.addEventListener('click',confirmRestart);
// when the DOM loads
let flipped = false;
let firstCard;
let secondCard;
let flippedNumber = 0;
let lockFlipping = false;
let score = 0;
let cardsMatched = 0;

createDivsForGifs(shuffledGifs);
storeBestScore(score, cardsMatched);
restart.addEventListener('click', restartGame);

startGame.addEventListener('click', () => {
  home.style.display = "none";
  gameStarted.style.display = "block";
})

playAgain.addEventListener('click', () => {
  winScreen.style.display = "none";
  gameStarted.style.display="block";
  bestScore.style.visibility="visible";
  reset();
  
})
